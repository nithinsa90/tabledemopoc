//
//  APIManager.swift
//  DemoPOC
//
//  Created by Nithin Samuel Abraham on 10/07/18.
//  Copyright © 2018 Nithin Samuel Abraham. All rights reserved.
//

import Foundation
import Alamofire

////////////////////////////////////////////////////////////////////
//Protocols for custom delegate
////////////////////////////////////////////////////////////////////
@objc protocol APIManagerDelegate: class{
    @objc optional func didFinishLoadingData(data: String)
    @objc optional func didFailWithError(error: String)
    
}

class APIManager
    
{
    static let shared = APIManager()
    private init() {
        
    }
    ////////////////////////////////////////////////////////////////////
    //Method to fetch country data from the json
    ////////////////////////////////////////////////////////////////////
    func fetchCountryDetails(delegate_:APIManagerDelegate)    {
        
        Alamofire.request(Constants.WebserviceConstants.API).validate()
            .responseString{
                response in
                print(response)
                if let json = response.result.value {
                    delegate_.didFinishLoadingData!(data: json)
                    print(json)
                }
                else{
                    let err = response.result.error
                    delegate_.didFailWithError!(error: (err?.localizedDescription)!)
                    print(err as Any)
                }
        }
    }
    
}
