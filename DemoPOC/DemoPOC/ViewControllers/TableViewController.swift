//
//  TableViewController.swift
//  DemoPOC
//
//  Created by Nithin Samuel Abraham on 10/07/18.
//  Copyright © 2018 Nithin Samuel Abraham. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
import ObjectMapper
import SDWebImage


class TViewController: UITableViewController,APIManagerDelegate {
    var TableDataSourceArray = NSMutableArray()
    var TitleText : String = ""
    private var refreshControler: UIRefreshControl!
    override func viewDidLoad() {
        
        CreateUI()
        GetData()
    }
    
    ////////////////////////////////////////////////////////////////////
    //In this method, we define the UI elements and set their properties
    ////////////////////////////////////////////////////////////////////
    func CreateUI() {
        
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        self.tableView.rowHeight = UITableViewAutomaticDimension
        //Creating button for refresh action
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.refresh, target: self, action:#selector(RefreshTapped))
        self.tableView.allowsSelection = false
        
    }
    
    ////////////////////////////////////////////////////////////////////
    //Method to invoke APIManager instance to fetch json data
    ////////////////////////////////////////////////////////////////////
    func GetData()
    {
        if(Connectivity.isConnectedToInternet()){
            APIManager.shared.fetchCountryDetails(delegate_: self)
        }
        else{
            self .DiplayAlert()
        }
    }
    @IBAction func RefreshTapped(_ sender: Any) {
        if(Connectivity.isConnectedToInternet()){
            DispatchQueue.main.async (
                execute:{
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            )
            //Fetching the country data again on refresh
            APIManager.shared.fetchCountryDetails(delegate_: self)
            self.tableView.reloadData()
        }
        else{
            self .DiplayAlert()
        }
        
    }
    ////////////////////////////////////////////////////////////////////
    //Method to rescale the images
    ////////////////////////////////////////////////////////////////////
    func RescaleImage(with image: UIImage?, scaledTo newSize: CGSize) -> UIImage? {
        var newSize = newSize
        let size: CGSize? = image?.size
        //Getting the width/height ratio
        let widthRatio: CGFloat = newSize.width / (image?.size.width ?? 0.0)
        let heightRatio: CGFloat = newSize.height / (image?.size.height ?? 0.0)
        //Checking device orientation
        if widthRatio > heightRatio {
            newSize = CGSize(width: (size?.width ?? 0.0) * heightRatio, height: (size?.height ?? 0.0) * heightRatio)
        } else {
            newSize = CGSize(width: (size?.width ?? 0.0) * widthRatio, height: (size?.height ?? 0.0) * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image?.draw(in: rect)
        let newImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func didFinishLoadingData(data: String) {
        //Mapping the response json string to model class
        let countryData = Mapper<CountryAPiResponse>().map(JSONString: data)!
        TableDataSourceArray = NSMutableArray()
        self.TitleText = countryData.title!
        for data in (countryData.rows)!{
            
            TableDataSourceArray.add(data)
        }
        //Refreshing the table view
        self.tableView.reloadData()
        SVProgressHUD.dismiss()
        
    }
    ////////////////////////////////////////////////////////////////////
    //Method to display an alert if not connected to the internet
    ////////////////////////////////////////////////////////////////////
    func DiplayAlert(){
        let alert = UIAlertController(title: Constants.AlertMessages.ALERT, message: Constants.AlertMessages.NOT_CONNECTED, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: Constants.AlertMessages.OK, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        if(refreshControler.isRefreshing){
            refreshControler .endRefreshing()
        }
    }
    // MARK: TableView Delegate Methods
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = "Identifier"
        var cell:UITableViewCell? =
            tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)
        if (cell == nil){
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                                   reuseIdentifier: reuseIdentifier)
            cell?.detailTextLabel?.textColor = UIColor.black
            cell?.detailTextLabel?.numberOfLines = 0
            cell?.detailTextLabel?.adjustsFontSizeToFitWidth = true
            cell?.detailTextLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            cell?.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            cell?.clipsToBounds = true
            
        }
        
        let countryData: Row = TableDataSourceArray[indexPath.row] as! Row
        //Checking if title text is empty
        if(countryData.title != nil){
            cell?.textLabel?.text = countryData.title
        }
        else{
            cell?.textLabel?.text = Constants.AlertMessages.TITLE_NOT_PRESENT
        }
        //Checking if description text is empty
        if(countryData.desc != nil){
            cell?.detailTextLabel?.text = countryData.desc
        }
        else{
            cell?.detailTextLabel?.text = Constants.AlertMessages.DESCRIPTION_NOT_PRESENT
        }
        if let imageUrl = countryData.imageHref, countryData.imageHref != nil{
            let imgUrl = URL(string: imageUrl)
            cell?.imageView? .sd_setImage(with: imgUrl, completed: { image, error, cacheType, imageURL in
                //Checking if image is empty
                if ((image) != nil){
                    if UIDevice.current.model == Constants.Devices.IPAD_LOWER || UIDevice.current.model == Constants.Devices.IPAD{
                        cell?.imageView?.image = self .RescaleImage(with: image, scaledTo: CGSize(width: 100.0, height: 100.0))
                    }
                    else{
                        cell?.imageView?.image = self .RescaleImage(with: image, scaledTo: CGSize(width: 60.0, height: 60.0))
                    }
                }
                else{
                    cell?.imageView?.image = nil
                }
                cell?.layoutSubviews()
                cell?.setNeedsLayout()
            })
        }
        else{
            cell?.imageView?.image = nil
        }
        
        
        return cell!
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TableDataSourceArray.count
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.TitleText
    }
    
    
}
