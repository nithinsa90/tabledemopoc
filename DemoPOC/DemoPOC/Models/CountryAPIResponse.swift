//
//  CountryAPIResponse.swift
//  DemoPOC
//
//  Created by Nithin Samuel Abraham on 10/07/18.
//  Copyright © 2018 Nithin Samuel Abraham. All rights reserved.
//

import Foundation
import ObjectMapper
class CountryAPiResponse:Mappable{
    var title:String?
    var rows :[Row]?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        title <- map[Constants.Keys.TITLE]
        rows <- map[Constants.Keys.ROWS]
    }
}
class Row:Mappable{
    var title:String?
    var desc:String?
    var imageHref:String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        title <- map[Constants.Keys.TITLE]
        desc <- map[Constants.Keys.DESCRIPTION]
        imageHref <- map[Constants.Keys.IMAGEREF]
    }
}
