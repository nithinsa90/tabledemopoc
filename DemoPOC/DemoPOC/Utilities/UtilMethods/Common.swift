//
//  Common.swift
//  DemoPOC
//
//  Created by Nithin Samuel Abraham on 10/07/18.
//  Copyright © 2018 Nithin Samuel Abraham. All rights reserved.
//

import Foundation
import Alamofire


/////////////////////////////////////////////////////////////
//Class to check Internet connectivity throughout the app
/////////////////////////////////////////////////////////////

class Connectivity {
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
