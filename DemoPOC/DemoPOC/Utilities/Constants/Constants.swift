//
//  Constants.swift
//  DemoPOC
//
//  Created by Nithin Samuel Abraham on 10/07/18.
//  Copyright © 2018 Nithin Samuel Abraham. All rights reserved.
//

import Foundation

///////////////////////////////////////////////
//Defining all constants to be used as structs
///////////////////////////////////////////////
struct Constants {
    struct WebserviceConstants {
        static let API = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json";
    }
    //Keys for Json data
    struct Keys {
        static let ROWS = "rows"
        static let TITLE = "title"
        static let DESCRIPTION = "description"
        static let IMAGEREF = "imageHref"
    }
    //Error and alert message strings
    struct  AlertMessages {
        static let TITLE_NOT_PRESENT = "Title Not Present"
        static let DESCRIPTION_NOT_PRESENT = "Description Not Present"
        static let ALERT = "Alert"
        static let NOT_CONNECTED = "You are not connected to the Internet"
        static let OK = "Ok"
    }
    //Device name strings
    struct  Devices {
        static let IPAD = "ipad"
        static let IPAD_LOWER = "iPad"
    }
    
}

